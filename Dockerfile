# Here's a super simple DockerFile for building our
# own version of the FastAPI app.


# Start with a specified version of an existing image
# from docker hub (or the local VLIZ registry)
# Avoid using :latest. You can never be sure if you've got the
# latest latest version, or if it's a good one, or what changes have been made
# eg: the current latest (Oct 2021) is python3.9. If we pull ":python3.9" at some
# undefined point in the future it should be the same. If we pull ":latest" at
# some future point it could be python4 and break everything! Our local machine might
# also think that it has the "latest" version but it's actually 3.7
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.9

# Following the instructions from:
# https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker
# Copy the python code from "./code/*" to the /app folder where it
# is expected.
COPY ./code /app

# We might want to add some labels into the image so that we can follow
# the metadata back along and discover some extra facts "what else did this
# author create": https://docs.docker.com/engine/reference/builder/

LABEL "version": "0.1"
LABEL "maintainer": "Joe Soap"
LABEL "description": "This is a simple fastAPI app to test CI/CD"
