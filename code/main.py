from fastapi import FastAPI

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "Hello World ver 2"}

@app.get("/message2")
async def message2():
    return {"other message": "This is number 2"}
