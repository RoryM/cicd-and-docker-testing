# CICD and Docker testing

## Purpose
This example project sets up a very basic use case for building docker images via Gitlab's CI/CD, storing the docker image in the gitlab registry and then to use that to deploy prebuilt "production" containers.

## Steps
* [Create a deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/). This gives read only access to the repository for machines that will be doing the deployment. Or you can just use the one saved in ./config/sample.env
* [Login with the docker engine](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry). This tells the engine where/how to pull images
* [Setup the CI/CD pipeline](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html). This tells gitlab what the process to follow is when receiving new commits to specific branches. 

## Coding process
* Bring up the docker-compose project with "docker-compose up"
* Make changes to the code. Use "docker-compose restart" to see the changes take effect on the "test" service
* Commit and push code changes. 
* Wait for the gitlab pipeline to finish building the new docker image (+-1 min)
* Pull new image with "docker-compose pull" and then "docker-compose restart" to see changes take effect on "production" service

